///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   22_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "helper.h"

#define PROGRAM_NAME "wc"
#define MAX_FILE_COUNT 256
#define MAX_FILENAME_LENGTH 256

/* Function Declarations */
void checkInput(int argc, const char * argv[]);
void checkInputFiles(void);
bool checkValidFlagWord(const char * str);
bool checkValidFlagChar(char ch);
void processFiles(void);
void processStdin(void);
void count(FILE * fp);
void printCurr(const char * filename);
void printTotal(void);
void printVersion(void);

/**
 Global Variables
 currLines, currWords, currChars are counters for the current file.
 totalLines, totalWords, totalChars are counters for the total statistics.
 The booleans keep track if the flags are present or not
*/
int currLines, currWords, currChars, totalLines, totalWords, totalChars, filesCount;
bool flagBytes, flagLines, flagWords, flagVersion, flagNone, inputFromStdin;


char files[MAX_FILE_COUNT][MAX_FILENAME_LENGTH];

/**
 Main driver
 */
int main(int argc, const char * argv[]) {
   totalChars = totalLines = totalWords = filesCount = 0;
   inputFromStdin = false;
   checkInput(argc, argv);
   if (inputFromStdin) {
      processStdin();
   } else {
      checkInputFiles();
      processFiles();
   }
   exit(EXIT_SUCCESS);
}

/**
 Checks if input is from stdin or from files via command line args.
 Checks if flags are set, and if they are valid
 Adds filenames to the files list to be processed
 */
void checkInput(int argc, const char * argv[]) {
   
   flagBytes = flagLines = flagWords = flagVersion = false;
   flagNone = true;
   
   for (int i=1; i<argc; i++) {
      if (argv[i][0] == '-') {
         if (argv[i][1] == '-') {
            if (!checkValidFlagWord(argv[i])) {
               fprintf(stderr, "wc: unrecognized option %s\n", argv[i]);
               exit(EXIT_FAILURE);
            }
         } else {
            int j = 1;
            while (argv[i][j] != '\0') {
               if (!checkValidFlagChar(argv[i][j])) {
                  fprintf(stderr, "wc: unrecognized option %c\n", argv[i][j]);
                  exit(EXIT_FAILURE);
               }
               j++;
            }
         }
      } else if (argv[i][0] != '-'){
         strcpy(files[filesCount], argv[i]);
         filesCount++;
      }
   }
   
   // if there is no input files
   if (filesCount == 0 && !flagVersion) {
      //if stdin is not a terminal (a file was provided)
      if (!isatty(STDIN_FILENO)) {
         inputFromStdin = true;
      } else if (argc == 1 && inputFromStdin==false) {
         fprintf(stderr,"Usage: wc FILE\n");
         exit(EXIT_FAILURE);
      }
   }
   
}

/**
 Checks if there are no input files from the command line arguments
 */
void checkInputFiles() {
   if (files[0][0] == '\0' && flagVersion) {
      printVersion();
      exit(EXIT_SUCCESS);
   } else if (files[0][0] == '\0') {  // print out version if there are no input files
      fprintf(stderr,"wc: no input files\n");
      exit(EXIT_FAILURE);
   }
}

/**
Checks if the string is a valid word flag
*/
bool checkValidFlagWord(const char * str) {
   if (strcmp(str,"--bytes")==0) {
      flagBytes = true;
      flagNone = false;
      return true;
   }
   if (strcmp(str,"--lines")==0) {
      flagLines = true;
      flagNone = false;
      return true;
   }
   if (strcmp(str,"--words")==0) {
      flagWords = true;
      flagNone = false;
      return true;
   }
   if (strcmp(str,"--version")==0) {
      flagVersion = true;
      flagNone = false;
      return true;
   }
   return false;
}

/**
 Checks if the character is a valid character flag
 */
bool checkValidFlagChar(char ch) {
   if (ch == 'c') {
      flagBytes = true;
      flagNone = false;
      return true;
   }
   if (ch == 'l') {
      flagLines = true;
      flagNone = false;
      return true;
   }
   if (ch == 'w') {
      flagWords = true;
      flagNone = false;
      return true;
   }
   return false;
}

/**
 Iterates through the files, counting and printing statistics for each one, and adding to the total.
 Then, it resets the current file statistics counter.
 */
void processFiles() {
   for (int i=0; i< filesCount; i++) {
      FILE *fp = fopen(files[i], "r");
      if (fp == NULL) {
         printf("wc: Can't open [%s]\n", files[i]);
         exit(EXIT_FAILURE);
      } else {
         // Initialize current counts
         currChars = 0; currWords = 0; currLines = 0;
         count(fp);
         printCurr(files[i]);
         totalChars+= currChars; totalWords+= currWords; totalLines+= currLines;
      }
   }
   
   if (filesCount > 1) {
    printTotal();
   }
}

/**
Processes input from stdin, counting and printing statistics.
*/
void processStdin() {
   currChars = 0; currWords = 0; currLines = 0;
   count(stdin);
   printCurr("stdin");
   totalChars+= currChars; totalWords+= currWords; totalLines+= currLines;
}

/**
 Main function that counts characters, words, lines in the current file.
 */
void count(FILE * fp){
   bool inWord = false;
   char curr;
   while ((curr = fgetc(fp)) != EOF) {
      currChars++;
      if (isWhitespace1(curr)){
         if (inWord) {
            inWord = false;
            currWords++;
         }
         if (isWhitespace2(curr)) currLines++;
      } else {
         inWord = true;
      }
   }
}

/**
 Print statistics for current file only
 */
void printCurr(const char * filename) {
   if (flagNone) {
      printf("%d\t%d\t%d\t[%s]\n", currLines, currWords, currChars, filename);
   } else {
      if (flagLines) printf("%d\t", currLines);
      if (flagWords) printf("%d\t", currWords);
      if (flagBytes) printf("%d\t", currChars);
      printf("[%s]\n", filename);
   }
}

/**
Print statistics of all files (total). If --version flag is set, prints version.
*/
void printTotal() {
   if (flagNone) {
      printf("%d\t%d\t%d\tTOTAL\n", totalLines, totalWords, totalChars);
   } else {
      if (flagLines) printf("%d\t", totalLines);
      if (flagWords) printf("%d\t", totalWords);
      if (flagBytes) printf("%d\t", totalChars);
      printf("TOTAL\n");
   }
   
   if (flagVersion) printVersion();
}

/**
 If --version flag is set, prints version.
 */
void printVersion() {
   printf("Version: wc 2.0\nWritten by Mirabela Medallon.\n");
}

