///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file helper.c
/// @version 2.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   22_01_2021
///////////////////////////////////////////////////////////////////////////////


#include "helper.h"

/**
 Returns true if the character is whitespace (not end of line)
*/
bool isWhitespace1(char ch) {
   if (ch == ' ' || ch == '\t' || ch == '\0' || ch == '\n' || ch == '\f' || ch == '\r' || ch == '\v') {
      return true;
   } else {
      return false;
   }
}

/**
 Returns true if the character the end of a line
 */
bool isWhitespace2(char ch) {
   if (ch == '\0' || ch == '\n') {
      return true;
   } else {
      return false;
   }
}

