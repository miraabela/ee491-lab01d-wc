///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file helper.h
/// @version 2.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   22_01_2021
///////////////////////////////////////////////////////////////////////////////


#ifndef helper_h
#define helper_h

#include <stdio.h>
#include <stdbool.h>

bool isWhitespace1(char ch);
bool isWhitespace2(char ch);
#endif /* helper_h */

