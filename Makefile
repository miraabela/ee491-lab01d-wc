# Build a Word Counting program

CC = gcc
CFLAGS = -Wall
LDFLAGS =
OBJFILES = helper.o wc.o
TARGET = wc
all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJFILES) $(LDFLAGS)

test:
	./$(TARGET) test1 test2 test3 test4
	./$(TARGET) --version
	./$(TARGET) -c test1
	./$(TARGET) --bytes test1
	./$(TARGET) -l test1
	./$(TARGET) --lines test1
	./$(TARGET) -w test1
	./$(TARGET) --words test1
	cal | ./$(TARGET)
	echo "test" | ./$(TARGET)

clean:
	rm -f $(OBJFILES) $(TARGET) *~

